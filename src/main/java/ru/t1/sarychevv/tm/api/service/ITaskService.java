package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void create(@Nullable String name, @Nullable String description);

}
