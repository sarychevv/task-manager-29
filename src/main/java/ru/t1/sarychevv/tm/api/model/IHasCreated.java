package ru.t1.sarychevv.tm.api.model;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHasCreated {

    @NotNull
    Date getCreated();

}
