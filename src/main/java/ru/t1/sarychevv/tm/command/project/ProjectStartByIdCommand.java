package ru.t1.sarychevv.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Start project by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-start-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getProjectService().changeStatusById(userId, id, Status.IN_PROGRESS);
    }

}
