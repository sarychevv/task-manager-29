package ru.t1.sarychevv.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.model.IAuthService;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.model.User;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Registry user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-registry";
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @NotNull final User user = authService.registry(login, password, email);
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
